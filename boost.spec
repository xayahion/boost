%global boost_docdir __tmp_docdir
%global boost_examplesdir __tmp_examplesdir
%global version_enc 1_77_0

%bcond_with mpich
%bcond_with openmpi
%bcond_without context
%bcond_without python3

%ifnarch %{ix86} x86_64
  %bcond_with quadmath
%else
  %bcond_without quadmath
%endif

%bcond_with tests
%bcond_with docs_generated

Name:           boost
Version:        1.77.0
Release:        4
Summary:        The free peer-reviewed portable C++ source libraries
License:        Boost Software License 1.0
URL:            http://www.boost.org
Source0:        https://boostorg.jfrog.io/ui/native/main/release/1.77.0/source/%{name}_%{version_enc}.tar.gz
Source1:        bjam

# https://bugzilla.redhat.com/show_bug.cgi?id=828856
# https://bugzilla.redhat.com/show_bug.cgi?id=828857
# https://svn.boost.org/trac/boost/ticket/6701
# https://github.com/boostorg/pool/pull/42
Patch1:		boost-1.77-pool-fix-interger-overflows-in-pool-ordered_malloc.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=1318383
Patch2:		boost-1.77-build-drop-rpath.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=1899888
# https://github.com/boostorg/locale/issues/52
Patch3:		boost-1.73-locale-empty-vector.patch

# https://github.com/boostorg/locale/pull/38
Patch4:		boost-1.77-locale-remove-linking-with-boost-system.patch

# https://github.com/boostorg/type_erasure/pull/19
Patch5:		boost-1.77-type_erasure-remove-boost-system-linkage.patch

Requires:       %{name}-atomic%{?_isa} = %{version}-%{release}
Requires:       %{name}-chrono%{?_isa} = %{version}-%{release}
Requires:       %{name}-container%{?_isa} = %{version}-%{release}
Requires:       %{name}-context%{?_isa} = %{version}-%{release}
Requires:       %{name}-coroutine%{?_isa} = %{version}-%{release}
Requires:       %{name}-date-time%{?_isa} = %{version}-%{release}
Requires:       %{name}-fiber%{?_isa} = %{version}-%{release}
Requires:       %{name}-filesystem%{?_isa} = %{version}-%{release}
Requires:       %{name}-graph%{?_isa} = %{version}-%{release}
Requires:       %{name}-iostreams%{?_isa} = %{version}-%{release}
Requires:       %{name}-locale%{?_isa} = %{version}-%{release}
Requires:       %{name}-log%{?_isa} = %{version}-%{release}
Requires:       %{name}-math%{?_isa} = %{version}-%{release}
Requires:       %{name}-nowide%{?_isa} = %{version}-%{release}
Requires:       %{name}-program-options%{?_isa} = %{version}-%{release}
Requires:       %{name}-random%{?_isa} = %{version}-%{release}
Requires:       %{name}-regex%{?_isa} = %{version}-%{release}
Requires:       %{name}-serialization%{?_isa} = %{version}-%{release}
Requires:       %{name}-stacktrace%{?_isa} = %{version}-%{release}
Requires:       %{name}-system%{?_isa} = %{version}-%{release}
Requires:       %{name}-test%{?_isa} = %{version}-%{release}
Requires:       %{name}-thread%{?_isa} = %{version}-%{release}
Requires:       %{name}-timer%{?_isa} = %{version}-%{release}
Requires:       %{name}-type_erasure%{?_isa} = %{version}-%{release}
Requires:       %{name}-wave%{?_isa} = %{version}-%{release}
Requires:       %{name}-contract%{?_isa} = %{version}-%{release}

BuildRequires:  gcc-c++
BuildRequires:  bzip2-devel zlib-devel libicu-devel
%if %{with python3}
BuildRequires:  python3-devel python3-numpy
%endif

%if %{with quadmath}
BuildRequires:  libquadmath-devel
%endif

Obsoletes: boost-signals

%description
Boost provides free peer-reviewed portable C++ source libraries. The
emphasis is on libraries which work well with the C++ Standard
Library. In order to establish "existing practice" and provide
reference implementations so that the Boost libraries are suitable
for eventual standardization. Ten Boost libraries are included in
the C++ Standards Committee's Library Technical Report (TR1) and in
the new C++11 Standard. C++11 also includes several more Boost
libraries in addition to those from TR1. More Boost libraries are
proposed for standardization in C++17.

%package contract
Summary: Boost.Contract runtime library

%description contract
Runtime support for Boost.Contract, a library that implements
Design by Contract or DbC or contract programming.

%package atomic
Summary: C++11-style atomic<>

%description atomic

Boost.Atomic is a library that provides atomic data types and
operations on these data types, as well as memory ordering constraints
required for coordinating multiple threads through atomic variables.
It implements the interface as defined by the C++11 standard,but makes
this feature available for platforms lacking system/compiler support
for this particular C++11 feature.

%package chrono
Summary: Useful time utilities C++11

%description chrono

Boost.Chrono implements the new time facilities in C++11, as proposed in
N2661 - A Foundation to Sleep On. That document provides background and
motivation for key design decisions and is the source of a good deal of
information in this documentation. In addition to the clocks provided by
the standard proposal, Boost.Chrono provides specific process and thread
clocks.

%package container
Summary: Standard library containers and extensions

%description container

Boost.Container library implements several well-known containers,
including STL containers. The aim of the library is to offer advanced
features not present in standard containers or to offer the latest
standard draft features for compilers that don't comply with the latest
C++.

%package context
Summary: (C++11) Context switching library

%description context

Boost.Context is a foundational library that provides a sort of
cooperative multitasking on a single thread. By providing an abstraction
of the current execution state in the current thread, including the stack
(with local variables) and stack pointer, all registers and CPU flags,
and the instruction pointer, a execution context represents a specific
point in the application's execution path. This is useful for building
higher-level abstractions, like coroutines, cooperative threads
(userland threads) or an equivalent to C# keyword yield in C++.

%package coroutine
Summary: Run-time component of boost coroutine library

%description coroutine
Boost.Coroutine provides templates for generalized subroutines which
allow suspending and resuming execution at certain locations. It
preserves the local state of execution and allows re-entering
subroutines more than once (useful if state must be kept across function
calls).

%package date-time
Summary: A set of date-time libraries based on generic programming concepts

%description date-time

Boost Date Time is a set of date-time libraries based on generic
programming concepts.

%package fiber
Summary: (C++11) Userland threads library

%description fiber

Boost.Fiber provides a framework for micro-/userland-threads (fibers)
scheduled cooperatively. The API contains classes and functions to manage
and synchronize fibers similiarly to standard thread support library.

%package filesystem
Summary: Run-time component of boost filesystem library

%description filesystem

The Boost Filesystem Library provides portable facilities to query and
manipulate paths, files, and directories.

%package graph
Summary: Run-time component of boost graph library
Requires: boost-regex%{?_isa} = %{version}-%{release}

%description graph

BGL interface and graph components are generic, in the same sense as
the Standard Template Library (STL).

%package iostreams
Summary: Run-time component of boost iostreams library

%description iostreams

Boost.Iostreams provides a framework for defining streams, stream
buffers and i/o filters.

%package json
Summary: Run-time component of boost json library

%description json

Run-time support for Boost.Json, a portable C++ library which provides
containers and algorithms that implement JavaScript Object Notation, or
simply "JSON"

%package locale
Summary: Run-time component of boost locale library
Requires: boost-chrono%{?_isa} = %{version}-%{release}
Requires: boost-thread%{?_isa} = %{version}-%{release}

%description locale

Boost.Locale provide a set of localization and Unicode handling tools.

%package log
Summary: Run-time component of boost logging library

%description log

Boost.Log library aims to make logging significantly easier for the
application developer.  It provides a wide range of out-of-the-box
tools along with public interfaces for extending the library.

%package math
Summary: Math functions for boost TR1 library

%description math

Boost.Math includes several contributions in the domain of mathematics:
The Greatest Common Divisor and Least Common Multiple library provides
run-time and compile-time evaluation of the greatest common divisor
(GCD) or least common multiple (LCM) of two integers. The Special
Functions library currently provides eight templated special functions,
in namespace boost. The Complex Number Inverse Trigonometric Functions
are the inverses of trigonometric functions currently present in the C++
standard. Quaternions are a relative of complex numbers often used to
parameterise rotations in three dimentional space. Octonions, like
quaternions, are a relative of complex numbers.

%package nowide
Summary: Standard library functions with UTF-8 API on Windows

%description nowide

Run-time support for Boost.Nowide.

%if %{with python3}

%package numpy3
Summary: Run-time component of boost numpy library for Python 3
Requires: boost-python3%{?_isa} = %{version}-%{release}
Requires: python3-numpy

%description numpy3

The Boost Python Library is a framework for interfacing Python and
C++. It allows you to quickly and seamlessly expose C++ classes,
functions and objects to Python, and vice versa, using no special
tools -- just your C++ compiler.  This package contains run-time
support for the NumPy extension of the Boost Python Library for Python 3.

%package python3
Summary: Run-time component of boost python library for Python 3

%description python3

The Boost Python Library is a framework for interfacing Python and
C++. It allows you to quickly and seamlessly expose C++ classes,
functions and objects to Python, and vice versa, using no special
tools -- just your C++ compiler.  This package contains run-time
support for the Boost Python Library compiled for Python 3.

%package python3-devel
Summary: Shared object symbolic links for Boost.Python 3
Requires: boost-numpy3%{?_isa} = %{version}-%{release}
Requires: boost-python3%{?_isa} = %{version}-%{release}
Requires: boost-devel%{?_isa} = %{version}-%{release}

%description python3-devel

Shared object symbolic links for Python 3 variant of Boost.Python.

%endif

%package program-options
Summary:  Run-time component of boost program_options library

%description program-options

Boost program options library allows program developers to obtain
(name, value) pairs from the user, via conventional methods such as
command line and config file.

%package random
Summary: A complete system for random number generation

%description random

The Boost Random Number Library provides a variety of generators and
distributions to produce random numbers having useful properties,
such as uniform distribution.

%package regex
Summary: Run-time component of boost regular expression library

%description regex

Regular expression library.

%package serialization
Summary: Run-time component of boost serialization library

%description serialization

Run-time support for serialization for persistence and marshaling.

%package stacktrace
Summary: Run-time component of boost stacktrace library

%description stacktrace

Gather, store, copy and print backtraces.

%package system
Summary: Run-time component of boost system support library

%description system

Boost operating system support library, including the diagnostics support
that will be part of the C++0x standard library.

%package test
Summary: Run-time component of boost test library

%description test

Support for simple program testing, full unit testing, and for program
execution monitoring.

%package thread
Summary: Run-time component of boost thread library

%description thread

Boost.Thread enables the use of multiple threads of execution with shared
data in portable C++ code. It provides classes and functions for managing
the threads themselves, along with others for synchronizing data between
the threads or providing separate copies of data specific to individual
threads.

%package timer
Summary: Event timer, progress timer, and progress display classes
Requires: boost-chrono%{?_isa} = %{version}-%{release}

%description timer

"How long does my C++ code take to run?"
The Boost Timer library answers that question and does so portably,
with as little as one #include and one additional line of code.

%package type_erasure
Summary: Run-time component of boost type erasure library
Requires: boost-chrono%{?_isa} = %{version}-%{release}

%description type_erasure

The Boost.TypeErasure library provides runtime polymorphism in C++
that is more flexible than that provided by the core language.

%package wave
Summary: Run-time component of boost C99/C++ preprocessing library
Requires: boost-chrono%{?_isa} = %{version}-%{release}
Requires: boost-filesystem%{?_isa} = %{version}-%{release}
Requires: boost-thread%{?_isa} = %{version}-%{release}

%description wave

The Boost.Wave library is a Standards conforming, and highly
configurable implementation of the mandated C99/C++ preprocessor
functionality packed behind an easy to use iterator interface.

%package devel
Summary: The Boost C++ headers, shared and static development libraries
Requires: boost%{?_isa} = %{version}-%{release}
Requires: libicu-devel%{?_isa}
%if %{with quadmath}
Requires: libquadmath-devel%{?_isa}
%endif
Provides: boost-static
Obsoletes: boost-static

%description devel
Headers shared object symbolic links for the Boost C++ libraries and static
Boost C++ libraries distributed with boost.

%package help
Summary: HTML documentation for the Boost C++ libraries
BuildArch: noarch

%description help
This package contains the documentation in the HTML format of the Boost C++
libraries. The documentation provides the same content as that on the Boost
web page (http://www.boost.org/doc/libs/%{version_enc}).

%package examples
Summary: Source examples for the Boost C++ libraries
BuildArch: noarch
Requires: boost-devel = %{version}-%{release}

%description examples
This package contains example source files distributed with boost.

%if 0%{with openmpi}
%package openmpi
Summary: Run-time component of Boost.MPI library
BuildRequires: openmpi-devel
Requires: boost-serialization%{?_isa} = %{version}-%{release}

%description openmpi

Run-time support for Boost.MPI-OpenMPI, a library providing a clean C++
API over the OpenMPI implementation of MPI.

%package openmpi-devel
Summary: Shared library symbolic links for Boost.MPI
Requires: boost-devel%{?_isa} = %{version}-%{release}
Requires: boost-openmpi%{?_isa} = %{version}-%{release}
Requires: boost-graph-openmpi%{?_isa} = %{version}-%{release}

%description openmpi-devel

Devel package for Boost.MPI-OpenMPI, a library providing a clean C++
API over the OpenMPI implementation of MPI.

%if %{with python3}

%package openmpi-python3
Summary: Python 3 run-time component of Boost.MPI library
Requires: boost-openmpi%{?_isa} = %{version}-%{release}
Requires: boost-python3%{?_isa} = %{version}-%{release}
Requires: boost-serialization%{?_isa} = %{version}-%{release}
Requires: python3-openmpi%{?_isa}

%description openmpi-python3

Python 3 support for Boost.MPI-OpenMPI, a library providing a clean C++
API over the OpenMPI implementation of MPI.

%package openmpi-python3-devel
Summary: Shared library symbolic links for Boost.MPI Python 3 component
Requires: boost-devel%{?_isa} = %{version}-%{release}
Requires: boost-python3-devel%{?_isa} = %{version}-%{release}
Requires: boost-openmpi-devel%{?_isa} = %{version}-%{release}
Requires: boost-openmpi-python3%{?_isa} = %{version}-%{release}

%description openmpi-python3-devel

Devel package for the Python 3 interface of Boost.MPI-OpenMPI, a library
providing a clean C++ API over the OpenMPI implementation of MPI.

%endif

%package graph-openmpi
Summary: Run-time component of parallel boost graph library
Requires: boost-openmpi%{?_isa} = %{version}-%{release}
Requires: boost-serialization%{?_isa} = %{version}-%{release}

%description graph-openmpi

Run-time support for the Parallel BGL graph library.  The interface and
graph components are generic, in the same sense as the Standard
Template Library (STL).  This libraries in this package use OpenMPI
back-end to do the parallel work.
%endif

%if 0%{with mpich}
%package mpich
Summary: Run-time component of Boost.MPI library
BuildRequires: mpich-devel
Requires: boost-serialization%{?_isa} = %{version}-%{release}

%description mpich

Run-time support for Boost.MPI-MPICH, a library providing a clean C++
API over the MPICH implementation of MPI.

%package mpich-devel
Summary: Shared library symbolic links for Boost.MPI
Requires: boost-devel%{?_isa} = %{version}-%{release}
Requires: boost-mpich%{?_isa} = %{version}-%{release}
Requires: boost-graph-mpich%{?_isa} = %{version}-%{release}

%description mpich-devel

Devel package for Boost.MPI-MPICH, a library providing a clean C++
API over the MPICH implementation of MPI.

%if %{with python3}

%package mpich-python3
Summary: Python 3 run-time component of Boost.MPI library
Requires: boost-mpich%{?_isa} = %{version}-%{release}
Requires: boost-python3%{?_isa} = %{version}-%{release}
Requires: boost-serialization%{?_isa} = %{version}-%{release}
Requires: python3-mpich%{?_isa}

%description mpich-python3

Python 3 support for Boost.MPI-MPICH, a library providing a clean C++
API over the MPICH implementation of MPI.

%package mpich-python3-devel
Summary: Shared library symbolic links for Boost.MPI Python 3 component
Requires: boost-devel%{?_isa} = %{version}-%{release}
Requires: boost-python3-devel%{?_isa} = %{version}-%{release}
Requires: boost-mpich-devel%{?_isa} = %{version}-%{release}
Requires: boost-mpich-python3%{?_isa} = %{version}-%{release}

%description mpich-python3-devel

Devel package for the Python 3 interface of Boost.MPI-MPICH, a library
providing a clean C++ API over the MPICH implementation of MPI.

%endif

%package graph-mpich
Summary: Run-time component of parallel boost graph library
Requires: boost-mpich%{?_isa} = %{version}-%{release}
Requires: boost-serialization%{?_isa} = %{version}-%{release}

%description graph-mpich

Run-time support for the Parallel BGL graph library.  The interface and
graph components are generic, in the same sense as the Standard
Template Library (STL).  This libraries in this package use MPICH
back-end to do the parallel work.

%endif

%package build
Summary: Cross platform build system for C++ projects
Requires: boost-jam
BuildArch: noarch

%description build
Boost.Build is an easy way to build C++ projects, everywhere. You name
your pieces of executable and libraries and list their sources.  Boost.Build
takes care about compiling your sources with the right options,
creating static and shared libraries, making pieces of executable, and other
chores -- whether you're using GCC, MSVC, or a dozen more supported
C++ compilers -- on Windows, OSX, Linux and commercial UNIX systems.

%package doctools
Summary: Tools for working with Boost documentation
Requires: docbook-dtds
Requires: docbook-style-xsl

%description doctools

Tools for working with Boost documentation in BoostBook or QuickBook format.

%package jam
Summary: A low-level build tool

%description jam
Boost.Jam (BJam) is the low-level build engine tool for Boost.Build.
Historically, Boost.Jam is based on on FTJam and on Perforce Jam but has grown
a number of significant features and is now developed independently.

%prep
%autosetup -p1 -n %{name}_%{version_enc}

%build
%if %{with python3}
PYTHON3_ABIFLAGS=$(/usr/bin/python3-config --abiflags)
: PYTHON3_VERSION=%{python3_version}
: PYTHON3_ABIFLAGS=${PYTHON3_ABIFLAGS}
%endif

export RPM_OPT_FLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing -Wno-unused-local-typedefs -Wno-deprecated-declarations"
export RPM_LD_FLAGS

cat > ./tools/build/src/user-config.jam << "EOF"
import os ;
local RPM_OPT_FLAGS = [ os.environ RPM_OPT_FLAGS ] ;
local RPM_LD_FLAGS = [ os.environ RPM_LD_FLAGS ] ;

using gcc : : : <compileflags>$(RPM_OPT_FLAGS) <linkflags>$(RPM_LD_FLAGS) ;
%if 0%{with openmpi} || 0%{with mpich}
using mpi ;
%endif
EOF

%if %{with python3}
cat >> ./tools/build/src/user-config.jam << EOF
using python : %{python3_version} : /usr/bin/python3 : /usr/include/python%{python3_version}${PYTHON3_ABIFLAGS} : : : ;
EOF
%endif

./bootstrap.sh --with-toolset=gcc --with-icu

echo ============================= build serial ==================
./b2 -d+2 -q %{?_smp_mflags} \
  --without-mpi --without-graph_parallel --build-dir=serial \
%if !%{with context}
    --without-context --without-coroutine \
    --without-fiber \
%endif
  variant=release threading=multi debug-symbols=on pch=off \
%if %{with python3}
    python=%{python3_version} \
%endif
  stage

if [ $(find serial -type f -name has_atomic_flag_lockfree -print -quit | wc -l) -ne 0 ]; then
  DEF=D
else
  DEF=U
fi

%if %{with openmpi} || %{with mpich}
module purge ||:
%endif

%if %{with openmpi}
%{_openmpi_load}

%if %{with python3}
echo ============================= build $MPI_COMPILER ==================
./b2 -d+2 -q %{?_smp_mflags} \
    --with-mpi --with-graph_parallel --build-dir=$MPI_COMPILER \
    variant=release threading=multi debug-symbols=on pch=off \
    python=%{python3_version} stage
%endif

%{_openmpi_unload}
export PATH=/bin${PATH:+:}$PATH
%endif

%if %{with mpich}
%{_mpich_load}

%if %{with python3}
echo ============================= build $MPI_COMPILER ==================
./b2 -d+2 -q %{?_smp_mflags} \
    --with-mpi --with-graph_parallel --build-dir=$MPI_COMPILER \
    variant=release threading=multi debug-symbols=on pch=off \
    python=%{python3_version} stage
%endif

%{_mpich_unload}
export PATH=/bin${PATH:+:}$PATH
%endif

echo ============================= build Boost.Build ==================
(cd tools/build
 ./bootstrap.sh --with-toolset=gcc)

%check
:

%install
%if 0%{with openmpi} || 0%{with mpich}
module purge ||:
%endif


%if 0%{with openmpi}
%{_openmpi_load}

%if %{with python3}
echo ============================= install $MPI_COMPILER ==================
./b2 -q %{?_smp_mflags} \
  --with-mpi --with-graph_parallel --build-dir=$MPI_COMPILER \
  --stagedir=${RPM_BUILD_ROOT}${MPI_HOME} \
  variant=release threading=multi debug-symbols=on pch=off \
  python=%{python3_version} stage

mkdir -p ${RPM_BUILD_ROOT}%{python3_sitearch}/openmpi/boost
touch ${RPM_BUILD_ROOT}%{python3_sitearch}/openmpi/boost/__init__.py
mv ${RPM_BUILD_ROOT}${MPI_HOME}/lib/boost-python%{python3_version}/mpi.so \
   ${RPM_BUILD_ROOT}%{python3_sitearch}/openmpi/boost/
%endif

rm -f ${RPM_BUILD_ROOT}${MPI_HOME}/lib/libboost_{python,{w,}serialization}*
rm -f ${RPM_BUILD_ROOT}${MPI_HOME}/lib/libboost_numpy*
rm -r ${RPM_BUILD_ROOT}${MPI_HOME}/lib/cmake

%{_openmpi_unload}
export PATH=/bin${PATH:+:}$PATH
%endif

%if 0%{with mpich}
%{_mpich_load}

%if %{with python3}
echo ============================= install $MPI_COMPILER ==================
./b2 -q %{?_smp_mflags} \
  --with-mpi --with-graph_parallel --build-dir=$MPI_COMPILER \
  --stagedir=${RPM_BUILD_ROOT}${MPI_HOME} \
  variant=release threading=multi debug-symbols=on pch=off \
  python=%{python3_version} stage

mkdir -p ${RPM_BUILD_ROOT}%{python3_sitearch}/mpich/boost
touch ${RPM_BUILD_ROOT}%{python3_sitearch}/mpich/boost/__init__.py
mv ${RPM_BUILD_ROOT}${MPI_HOME}/lib/boost-python%{python3_version}/mpi.so \
   ${RPM_BUILD_ROOT}%{python3_sitearch}/mpich/boost/
%endif

rm -f ${RPM_BUILD_ROOT}${MPI_HOME}/lib/libboost_{python,{w,}serialization}*
rm -f ${RPM_BUILD_ROOT}${MPI_HOME}/lib/libboost_numpy*
rm -r ${RPM_BUILD_ROOT}${MPI_HOME}/lib/cmake

%{_mpich_unload}
export PATH=/bin${PATH:+:}$PATH
%endif

echo ============================= install serial ==================
./b2 -d+2 -q %{?_smp_mflags} \
  --without-mpi --without-graph_parallel --build-dir=serial \
%if !%{with context}
    --without-context --without-coroutine \
    --without-fiber \
%endif
  --prefix=$RPM_BUILD_ROOT%{_prefix} \
  --libdir=$RPM_BUILD_ROOT%{_libdir} \
  variant=release threading=multi debug-symbols=on pch=off \
%if %{with python3}
    python=%{python3_version} \
%endif
  install

[ -f $RPM_BUILD_ROOT%{_libdir}/libboost_thread.so ]
rm -f $RPM_BUILD_ROOT%{_libdir}/libboost_thread.so

rm -r $RPM_BUILD_ROOT/%{_libdir}/cmake

version=%{version}

echo ============================= install Boost.Build ==================
(cd tools/build
 ./b2 --prefix=$RPM_BUILD_ROOT%{_prefix} install
 chmod +x $RPM_BUILD_ROOT%{_datadir}/boost-build/src/tools/doxproc.py
 sed -i '1s@^#!/usr/bin.python$@&3@' $RPM_BUILD_ROOT%{_datadir}/boost-build/src/tools/doxproc.py
 rm $RPM_BUILD_ROOT%{_datadir}/boost-build/src/tools/doxygen/windows-paths-check.hpp
 rm -f $RPM_BUILD_ROOT%{_datadir}/boost-build/src/tools/doxygen/windows-paths-check.hpp
 rm -f $RPM_BUILD_ROOT%{_bindir}/b2

 install -m 755 %{_builddir}/%{name}_%{version_enc}/tools/build/b2 $RPM_BUILD_ROOT%{_bindir}/bjam
 %{__install} -p -m 644 %{SOURCE1} -D $RPM_BUILD_ROOT%{_mandir}/man1/bjam.1
)

echo ============================= install Boost.QuickBook ==================
(cd tools/quickbook
 ../build/b2 --prefix=$RPM_BUILD_ROOT%{_prefix}
 %{__install} -p -m 755 ../../dist/bin/quickbook $RPM_BUILD_ROOT%{_bindir}/
 cd ../boostbook
 find dtd -type f -name '*.dtd' | while read tobeinstalledfiles; do
   install -p -m 644 $tobeinstalledfiles -D $RPM_BUILD_ROOT%{_datadir}/boostbook/$tobeinstalledfiles
 done
 find xsl -type f | while read tobeinstalledfiles; do
   install -p -m 644 $tobeinstalledfiles -D $RPM_BUILD_ROOT%{_datadir}/boostbook/$tobeinstalledfiles
 done
)

echo ============================= install documentation ==================
rm -rf %{boost_docdir} && %{__mkdir_p} %{boost_docdir}/html
DOCPATH=%{boost_docdir}
DOCREGEX='.*\.\(html?\|css\|png\|gif\)'

find libs doc more -type f -regex $DOCREGEX | sed -n '/\//{s,/[^/]*$,,;p}' | sort -u > tmp-doc-directories

sed "s:^:$DOCPATH/:" tmp-doc-directories | xargs -P 0 --no-run-if-empty %{__install} -d

cat tmp-doc-directories | while read tobeinstalleddocdir; do
  find $tobeinstalleddocdir -mindepth 1 -maxdepth 1 -regex $DOCREGEX -print0 \
  | xargs -P 0 -0 %{__install} -p -m 644 -t $DOCPATH/$tobeinstalleddocdir
done
rm -f tmp-doc-directories
%{__install} -p -m 644 -t $DOCPATH LICENSE_1_0.txt index.htm index.html boost.png rst.css boost.css

echo ============================= install examples ==================
sed -i -e 's/\r//g' libs/geometry/example/ml02_distance_strategy.cpp
for tmp_doc_file in flyweight/example/Jamfile.v2 \
  format/example/sample_new_features.cpp multi_index/example/Jamfile.v2 \
  multi_index/example/hashed.cpp serialization/example/demo_output.txt
do
  mv libs/${tmp_doc_file} libs/${tmp_doc_file}.iso8859
  iconv -f ISO8859-1 -t UTF8 < libs/${tmp_doc_file}.iso8859 > libs/${tmp_doc_file}
  touch -r libs/${tmp_doc_file}.iso8859 libs/${tmp_doc_file}
  rm -f libs/${tmp_doc_file}.iso8859
done

rm -rf %{boost_examplesdir} && mkdir -p %{boost_examplesdir}/html
EXAMPLESPATH=%{boost_examplesdir}
find libs -type d -name example -exec find {} -type f \; | sed -n '/\//{s,/[^/]*$,,;p}' | sort -u > tmp-doc-directories
sed "s:^:$EXAMPLESPATH/:" tmp-doc-directories | xargs -P 0 --no-run-if-empty %{__install} -d
rm -f tmp-doc-files-to-be-installed && touch tmp-doc-files-to-be-installed
cat tmp-doc-directories | while read tobeinstalleddocdir
do
  find $tobeinstalleddocdir -mindepth 1 -maxdepth 1 -type f >> tmp-doc-files-to-be-installed
done
cat tmp-doc-files-to-be-installed | while read tobeinstalledfiles
do
  if test -s $tobeinstalledfiles; then
    tobeinstalleddocdir=`dirname $tobeinstalledfiles`
    %{__install} -p -m 644 -t $EXAMPLESPATH/$tobeinstalleddocdir $tobeinstalledfiles
  fi
done
rm -f tmp-doc-files-to-be-installed
rm -f tmp-doc-directories
%{__install} -p -m 644 -t $EXAMPLESPATH LICENSE_1_0.txt

%post doctools
CATALOG=%{_sysconfdir}/xml/catalog
%{_bindir}/xmlcatalog --noout --add "rewriteSystem" \
  "http://www.boost.org/tools/boostbook/dtd" \
  "file://%{_datadir}/boostbook/dtd" $CATALOG
%{_bindir}/xmlcatalog --noout --add "rewriteURI" \
  "http://www.boost.org/tools/boostbook/dtd" \
  "file://%{_datadir}/boostbook/dtd" $CATALOG
%{_bindir}/xmlcatalog --noout --add "rewriteSystem" \
  "http://www.boost.org/tools/boostbook/xsl" \
  "file://%{_datadir}/boostbook/xsl" $CATALOG
%{_bindir}/xmlcatalog --noout --add "rewriteURI" \
  "http://www.boost.org/tools/boostbook/xsl" \
  "file://%{_datadir}/boostbook/xsl" $CATALOG

%postun doctools
if [ "$1" = 0 ]; then
  CATALOG=%{_sysconfdir}/xml/catalog
  %{_bindir}/xmlcatalog --noout --del "file://%{_datadir}/boostbook/dtd" $CATALOG
  %{_bindir}/xmlcatalog --noout --del "file://%{_datadir}/boostbook/xsl" $CATALOG
fi


%files
%license LICENSE_1_0.txt

%files contract
%license LICENSE_1_0.txt
%{_libdir}/libboost_contract.so.%{version}

%files atomic
%license LICENSE_1_0.txt
%{_libdir}/libboost_atomic.so.%{version}

%files chrono
%license LICENSE_1_0.txt
%{_libdir}/libboost_chrono.so.%{version}

%files container
%license LICENSE_1_0.txt
%{_libdir}/libboost_container.so.%{version}

%files context
%license LICENSE_1_0.txt
%{_libdir}/libboost_context.so.%{version}

%files coroutine
%license LICENSE_1_0.txt
%{_libdir}/libboost_coroutine.so.%{version}

%files date-time
%license LICENSE_1_0.txt
%{_libdir}/libboost_date_time.so.%{version}

%files fiber
%license LICENSE_1_0.txt
%{_libdir}/libboost_fiber.so.%{version}

%files filesystem
%license LICENSE_1_0.txt
%{_libdir}/libboost_filesystem.so.%{version}

%files graph
%license LICENSE_1_0.txt
%{_libdir}/libboost_graph.so.%{version}

%files iostreams
%license LICENSE_1_0.txt
%{_libdir}/libboost_iostreams.so.%{version}

%files json
%license LICENSE_1_0.txt
%{_libdir}/libboost_json.so.%{version}

%files locale
%license LICENSE_1_0.txt
%{_libdir}/libboost_locale.so.%{version}

%files log
%license LICENSE_1_0.txt
%{_libdir}/libboost_log.so.%{version}
%{_libdir}/libboost_log_setup.so.%{version}

%files math
%license LICENSE_1_0.txt
%{_libdir}/libboost_math_c99.so.%{version}
%{_libdir}/libboost_math_c99f.so.%{version}
%{_libdir}/libboost_math_c99l.so.%{version}
%{_libdir}/libboost_math_tr1.so.%{version}
%{_libdir}/libboost_math_tr1f.so.%{version}
%{_libdir}/libboost_math_tr1l.so.%{version}

%files nowide
%license LICENSE_1_0.txt
%{_libdir}/libboost_nowide.so.%{version}

%if %{with python3}

%files numpy3
%license LICENSE_1_0.txt
%{_libdir}/libboost_numpy%{python3_version_nodots}.so.%{version}

%files python3
%license LICENSE_1_0.txt
%{_libdir}/libboost_python%{python3_version_nodots}.so.%{version}

%files python3-devel
%license LICENSE_1_0.txt
%{_libdir}/libboost_numpy%{python3_version_nodots}.so
%{_libdir}/libboost_python%{python3_version_nodots}.so

%endif

%files test
%license LICENSE_1_0.txt
%{_libdir}/libboost_prg_exec_monitor.so.%{version}
%{_libdir}/libboost_unit_test_framework.so.%{version}

%files program-options
%license LICENSE_1_0.txt
%{_libdir}/libboost_program_options.so.%{version}

%files random
%license LICENSE_1_0.txt
%{_libdir}/libboost_random.so.%{version}

%files regex
%license LICENSE_1_0.txt
%{_libdir}/libboost_regex.so.%{version}

%files serialization
%license LICENSE_1_0.txt
%{_libdir}/libboost_serialization.so.%{version}
%{_libdir}/libboost_wserialization.so.%{version}

%files stacktrace
%license LICENSE_1_0.txt
%{_libdir}/libboost_stacktrace_addr2line.so.%{version}
%{_libdir}/libboost_stacktrace_basic.so.%{version}
%{_libdir}/libboost_stacktrace_noop.so.%{version}

%files system
%license LICENSE_1_0.txt
%{_libdir}/libboost_system.so.%{version}

%files thread
%license LICENSE_1_0.txt
%{_libdir}/libboost_thread.so.%{version}

%files timer
%license LICENSE_1_0.txt
%{_libdir}/libboost_timer.so.%{version}

%files type_erasure
%license LICENSE_1_0.txt
%{_libdir}/libboost_type_erasure.so.%{version}

%files wave
%license LICENSE_1_0.txt
%{_libdir}/libboost_wave.so.%{version}

%files help
%doc %{boost_docdir}/*

%files devel
%exclude %{_libdir}/libboost_numpy%{python3_version_nodots}.so
%exclude %{_libdir}/libboost_python%{python3_version_nodots}.so
%license LICENSE_1_0.txt
%{_includedir}/%{name}
%{_libdir}/*.so
%{_libdir}/*.a
%if 0%{with mpich}
%{_libdir}/mpich/lib/*.a
%endif
%if 0%{with openmpi}
%{_libdir}/openmpi/lib/*.a
%endif

%files examples
%doc %{boost_examplesdir}/*

%if 0%{with openmpi}

%files openmpi
%license LICENSE_1_0.txt
%{_libdir}/openmpi/lib/libboost_mpi.so.%{version}

%files openmpi-devel
%license LICENSE_1_0.txt
%{_libdir}/openmpi/lib/libboost_mpi.so
%{_libdir}/openmpi/lib/libboost_graph_parallel.so

%if %{with python3}

%files openmpi-python3
%license LICENSE_1_0.txt
%{_libdir}/openmpi/lib/libboost_mpi_python%{python3_version_nodots}.so.%{version}
%{python3_sitearch}/openmpi/boost/

%files openmpi-python3-devel
%license LICENSE_1_0.txt
%{_libdir}/openmpi/lib/libboost_mpi_python%{python3_version_nodots}.so

%endif

%files graph-openmpi
%license LICENSE_1_0.txt
%{_libdir}/openmpi/lib/libboost_graph_parallel.so.%{version}
%endif

%if 0%{with mpich}
%files mpich
%license LICENSE_1_0.txt
%{_libdir}/mpich/lib/libboost_mpi.so.%{version}

%files mpich-devel
%license LICENSE_1_0.txt
%{_libdir}/mpich/lib/libboost_mpi.so
%{_libdir}/mpich/lib/libboost_graph_parallel.so

%if %{with python3}

%files mpich-python3
%license LICENSE_1_0.txt
%{_libdir}/mpich/lib/libboost_mpi_python%{python3_version_nodots}.so.%{version}
%{python3_sitearch}/mpich/boost/

%files mpich-python3-devel
%license LICENSE_1_0.txt
%{_libdir}/mpich/lib/libboost_mpi_python%{python3_version_nodots}.so

%endif

%files graph-mpich
%license LICENSE_1_0.txt
%{_libdir}/mpich/lib/libboost_graph_parallel.so.%{version}

%endif

%files build
%license LICENSE_1_0.txt
%{_datadir}/boost-build/

%files doctools
%license LICENSE_1_0.txt
%{_bindir}/quickbook
%{_datadir}/boostbook/

%files jam
%license LICENSE_1_0.txt
%{_bindir}/bjam
%{_mandir}/man1/bjam.1*

%changelog
* Wed Oct 06 2021 Liu Zixian <liuzixian4@huawei.com> - 1.77.0-4
- Remove dependencies on header-only libraries

* Sun Sep 26 2021 Liu Zixian <liuzixian4@huawei.com> - 1.77.0-3
- Remove linking script which is fixed in upstream PR266

* Wed Sep 22 2021 Liu Zixian <liuzixian4@huawei.com> - 1.77.0-2
- Correct license name for ci.

* Wed Sep 15 2021 Liu Zixian <liuzixian4@huawei.com> - 1.77.0-1
- update to 1.77.0

* Sat Jul 24 2021 Liu Zixian <liuzixian4@huawei.com> - 1.76.0-1
- update to 1.76.0

* Sat Jul 24 2021 Liu Zixian <liuzixian4@huawei.com> - 1.75.0-6
- remove needless BuildRequires

* Fri Jul 23 2021 zhouwenpei <zhouwenpei1@huawei.com> - 1.75.0-5
- remove useless buildrequires

* Wed Jun 23 2021 Xu Huijie <xuhuijie2@huawei.com> - 1.75.0-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix dead loop in parse_repeat()

* Fri Feb 5 2021 xinghe <xinghe1@huawei.com> - 1.75.0-3
- fix the conflict between the devel package file and the other subpackage file

* Thu Feb 4 2021 xinghe <xinghe1@huawei.com> - 1.75.0-2
- fix master build

* Wed Feb 3 2021 xinghe <xinghe1@huawei.com> - 1.75.0-1
- update version to 1.75.0

* Wed Dec 16 2020 xinghe <xinghe1@huawei.com> - 1.73.0-2
- correct license

* Fri Jul 24 2020 Wang Shuo<wangshuo47@huawei.com> - 1.73.0-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update boost to 1.73.0

* Mon Apr 20 2020 Wang Shuo<wangshuo47@huawei.com> - 1.72.0-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update boost to 1.72.0

* Thu Mar 19 2020 Yu Xiangyang<yuxiangyang4@huawei.com> - 1.66.0-18
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: fix build src.rpm error

* Tue Mar 10 2020 Wang Shuo<wangshuo47@huawei.com> - 1.66.0-17
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: move examples files to examples package

* Mon Oct 28 2019 caomeng <caomeng5@huawei.com> - 1.66.0-16
- Type:NA
- ID:NA
- SUG:NA
- DESC:add bcondwith openmpi and mpich

* Wed Aug 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.66.0-15
- Package init
